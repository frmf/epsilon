[![Build Status](https://gitlab.com/frmf/epsilon/badges/master/pipeline.svg)](https://gitlab.com/frmf/epsilon/-/pipelines?ref=master)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

---

Gitlab pages forked from [jekyll-example](https://gitlab.com/pages/jekyll).

---

https://frmf.gitlab.io/epsilon/


A learning journey...

## How to test locally

### First time serving 

`git clone <branch>`

`cd <branch-directory>`

`export JEKYLL_VERSION=3.8`

`docker run --name learning-journey --volume="$PWD:/srv/jekyll" -p 4000:4000 -it jekyll/jekyll:$JEKYLL_VERSION jekyll serve  --watch --drafts`

Once the 'learning-journey' container is running, the website is served at http://127.0.0.1:4000


### Starting, Stopping, and Logging

- Stop 'learning-journey' container:

    `docker stop learning-journey`

- (Re-) Start 'learning-journey' container:

    `docker start learning-journey`

- Get logs of 'learning-journey' container:

    `docker logs -f learning-journey`


References:

- https://github.com/envygeeks/jekyll-docker/blob/master/README.md
- https://ddewaele.github.io/running-jekyll-in-docker/

