---
layout: post
title:  "Find minimum by gradient descent"
date:   2020-12-30
categories: notebook
---

```python
from __future__ import print_function
import torch
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()

%matplotlib inline
```

# Find minimum of $$f(x) = x^2$$:

PyTorch ingredients:
- tensor `t`
    - `requires_grad=True`: enable autograd mechanism
    - `t.grad`: return gradient evaluated at tensor t
    - `t.grad.zero_()`: in-place zeroing of gradient
    - `t.backward()`: backpropagation


```python
def f(x):
    return x*x

c = 0.15 # interval boundary for plot
X = np.linspace(-c, c,20)

plt.figure(figsize=(9,9))
#fig = plt.figure(dpi=600)
plt.plot(X, f(X))

# initial minimum candidate
x = torch.tensor(-1.0, requires_grad=True)

n_steps = 15
learning_rate = 0.1

for step in range(1, n_steps+1):
    # calling backward will lead derivatives to accumulate at leaf nodes.
    # To prevent the accumulation at each iteration
    # we zero the gradient explicitly
    if x.grad is not None:
        x.grad.zero_()

    # Evaluate function at current x and do backpropagation, i.e. calculate df/dx(x):
    f(x).backward()

    # Update current x:
    # (make PyTorch autograd mechanism "look away" in the update step:
    # i.e. not add edges to the forward graph.)
    with torch.no_grad():
        x -= learning_rate * x.grad

    # plot only points that are inside interval [-c,c]
    if (np.absolute(x.detach().numpy()) <= c):
        plt.plot(x.detach().numpy(), f(x).detach().numpy(), 'X')

    print('Step %2d, minimum %f' %(step, float(x)))
```

    Step  1, minimum -0.800000
    Step  2, minimum -0.640000
    Step  3, minimum -0.512000
    Step  4, minimum -0.409600
    Step  5, minimum -0.327680
    Step  6, minimum -0.262144
    Step  7, minimum -0.209715
    Step  8, minimum -0.167772
    Step  9, minimum -0.134218
    Step 10, minimum -0.107374
    Step 11, minimum -0.085899
    Step 12, minimum -0.068719
    Step 13, minimum -0.054976
    Step 14, minimum -0.043980
    Step 15, minimum -0.035184





![png](https://gitlab.com/frmf/epsilon/-/raw/master/_posts/_plots/Learning_PyTorch_4_1.png)


